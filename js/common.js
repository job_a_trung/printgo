

// menu toggle
$(function() {
    var html = $('html, body'),
        navContainer = $('.nav-container'),
        navToggle = $('.nav-toggle'),
        navDropdownToggle = $('.has-dropdown');
        overlay =$("<div class='overlay'></div> ");
        overlay2 =$("<div class='overlay'></div> ");

    // Nav toggle
    navToggle.on('click', function(e) {
        overlay.toggle();
        var $this = $(this);
        e.preventDefault();
        $this.toggleClass('is-active');
        navContainer.toggleClass('is-visible');
        html.toggleClass('nav-open');
    });


    $( "body" ).prepend(overlay);
    overlay.click(function(){
        navToggle.trigger('click');
         // $(this).toggle();
    })

    $( "body" ).prepend(overlay2);
    overlay2.click(function(){
         $(this).toggle();
    })
    // Nav dropdown toggle
    navDropdownToggle.on('click', function() {
        var $this = $(this);
        $this.toggleClass('is-active').siblings().removeClass('is-active');
        if ($this.children('ul').hasClass('open-nav')) {
              $this.children('ul').removeClass('open-nav');
              $this.children('ul').slideUp(350);
          }
        else {
          $this.parent().parent().find('li .nav-dropdown').removeClass('open-nav');
          $this.parent().parent().find('li .nav-dropdown').slideUp(350);
          $this.children('ul').toggleClass('open-nav');
          $this.children('ul').slideToggle(350);
        }
    });

    // Prevent click events from firing on children of navDropdownToggle
    navDropdownToggle.on('click', '*', function(e) {
        e.stopPropagation();
    });


});
$( '.menu-item' ).click(function() {
    var url = window.location.href;
    $('.menu-item').not(this).removeClass('is-active');
    $(this ).addClass('is-active');
});
 // style img

 // scroll add class
if (window.innerWidth > 992) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $('.menu_header').addClass('fixed');
        } else{
            $('.menu_header').removeClass('fixed');
        }
    });
}
if (window.innerWidth < 991) {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $('.sticky-header').addClass('clearfix');
        } else {
            $('.sticky-header').removeClass('clearfix');
        }
    });
}


// btn_search
$(function () {
      // search dropdown button
      $('.btn_search').click(function (e) {
          overlay2.toggle();
          e.preventDefault();
          $(this).parents('.search_drop').find('.form_search').toggleClass('open')
      })
      $(document).click(function (event) {
          // Check if clicked outside target
          if (!($(event.target).closest(".search_drop").length)) {
              // Hide target
              $(".form_search").removeClass('open');

          }

      });
    });

// scrollspy ----->


// $(document).ready(function(){
//     $('a[href^="#"]').on('click',function (e) {
//         e.preventDefault();
//         var target = this.hash;
//         var $target = $(target);
//         $('html, body').stop().animate({
//             'scrollTop': $target.offset().top
//         },1000, 'swing', function () {
//             window.location.hash = target;
//         });
//     });
// });

//scroll to top button
// ----------- croll --------------//
(function($) {
    //Scroll to Top
    function headerStyle() {
        if($('.header').length){
            var windowpos = $(window).scrollTop();
            var scrollLink = $('.scroll-top');
            if (windowpos >= 185) {
                scrollLink.addClass('open');
            } else {
                scrollLink.removeClass('open');
            }
        }
    }
    headerStyle();
    // Scroll to Target
    if($('.scroll-to-target').length){
        $(".scroll-to-target").on('click', function() {
            var target = $(this).attr('data-target');
            // animate
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1000);

        });
    }

    $(window).on('scroll', function() {
        headerStyle();
    });


})(window.jQuery);

$(function () {
    $(".category").click(function(){
        $(".ul-lev2").slideToggle();
    });
    // news detail
    $(".displayTableContent").click(function(){
        $(".toc_list").slideToggle();
    });

    $('#cap2').glassCase({
        'isOverlayFullImage': false, 'capZType': 'out', 'isZCapEnabled': true, 'capZPos': 'bottom',
        'widthDisplay':470, 'heightDisplay': 430, 'colorIcons': '#000', 'zoomPosition': 'right','thumbsPosition': 'left',
        'isSlowZoom': true, 'isSlowLens': true, 'colorIcons': '#F15129', 'colorActiveThumb': '#F15129'
    });
    $('#cap2').parents('.glass-case').addClass('pull-right');
});

//views detail--------------------------------////
